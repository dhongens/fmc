<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'fmc');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'mktnS?s+a/}66/}M5cuT,p2um&K]mJE2Qf{pugR_6_gt+3D0kJ~~|HIxHupxQH;l');
define('SECURE_AUTH_KEY',  'D9y:3KeRT54R]jfd$>y4SoGRr5EvQ6_2{L@d/^|SW{>Hx5DtX[4]7?ANWaP;7P0+');
define('LOGGED_IN_KEY',    '&Vf|WCW,_=k[]Q$|0y|J3x^_|7;l>)F&2iqx=u:$s5DqR%VzrF*VB7cQ|i:X)cCP');
define('NONCE_KEY',        '|D-o<_0d7*$.9}fq*`JeO3)=7:g_,=iS+8;.Yx)|Izc  Xoz7jiV8|E$DR++9jm/');
define('AUTH_SALT',        ']?*@+[Xqkzq@G0*5u-XnYCDR6C^5Qpvp4+_{;:$u=a~$q+_P&!;GNlBDJSZ%1n*y');
define('SECURE_AUTH_SALT', 't]T!YD?;z,M+hUi@LC]VSn{sdj{HxO_|;|N+7>GN+^Lo.tT*cM+:6}NQwX{oX}-+');
define('LOGGED_IN_SALT',   'qN h{4s:xJ6k9p=2A_j!1t)~K4sMx:}3Yy!OMCO=ipVGqW]jWDifut14/~?Rx%wy');
define('NONCE_SALT',       '!Gi^clRF$dK^EB3>JK6bhZ.t:[#b<a%p%SyGE0iR.tZm*zZWl8}44xpjwn@rzo/(');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
