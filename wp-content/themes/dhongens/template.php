<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Untitled Document</title>
	<link href='http://fonts.googleapis.com/css?family=Amaranth' rel='stylesheet' type='text/css'>
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/m-styles.min.css" rel="stylesheet">
	<link href="css/m-buttons.min.css" rel="stylesheet">
	<link href="css/m-forms.min.css" rel="stylesheet">
	<link href="css/m-icons.min.css" rel="stylesheet">
	<link href="css/m-normalize.min.css" rel="stylesheet">
    <link href="css/dhongens.css" rel="stylesheet">
    
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/m-dropdown.min.js"></script>
    <script type="text/javascript" src="js/m-radio.min.js"></script>
    <script type="text/javascript" src="js/dhongens.js"></script>
    
    <style>
	.back-draft {
		background-image: url(img/flowerbg.jpg);
		background-position: center;
		background-size: 100%;
	}
	.circle-image {
		background-image: url('http://placehold.it/692/FF7FAA/FFFFFF/&text=Put+Image+Here+692x692');
	}
	.grass-bg {
		background-image: url('img/grassybg.jpg');
		background-size: 100%;
		background-position:bottom;
	}
	</style>
    
</head>

<body>

<div class="top-logo-section">
    <div class="container the-width">
        <div class="row">
            <div class="col-sm-4 logo-image">
                <img src="img/fmc_logo.png" />
            </div>
            <div class="col-sm-8 put-menu">
                <div class="menu-section">
                    <div class="headline-section cwhite">OUR PROMISE TO SOCIETY</div>
                    <div class="top-menu-section">
                        <ul>
                            <li><a href="index.html">HOME</a></li>
                            <li><a href="aboutus.html">LIVING OUR BRAND</a></li>
                            <li><a href="services.html">DELIVERING OUR BRAND</a></li>
                            <li><a href="contactus.html">PROMISE TO INSPIRE</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="blue-strip">
	<div class="container">
    	<div class="row">
        	<div class="col-sm-12">
            	<div class="blue-strip-title floatleft cwhite bs-font">Community News Headline Ticker</div>
                <a href="#"><div class="blue-strip-link floatright cblue bs-font"><img src="img/arrow-right-yellow.png"/> Add Event <img src="img/arrow-left-yellow.png"/></div></a>
            	<a href="#"><div class="blue-strip-readmore floatright cwhite small"><img src="img/arrow-down-blue.png" />Read More</div></a>
            </div>            
        </div>
    </div>
</div>

<div class="back-draft">
    <div class="container the-width">
        <div class="row">
            <div class="col-sm-4">
				
            </div>
            <div class="col-sm-8 put-menu">
                <div class="backdraft-readnow cwhite">
					<p>As a Fresneius family member, you have a powerful and important role to play. You are a representative of our company's commitment to renal health, community and our promise to positively impact the lives of patients and families affected by kidney disease. <a href="#">Read More</a></p>
                </div>
            </div>
        </div>
    </div>
    <div class="circleBase circle-text cwhite">
    	<div class="country-list"><a href="#"><img src="img/arrow-left.png" /></a>PHILIPPINES<a href="#"><img src="img/arrow-right.png" /></a></div>
        <div class="event-top-title center-block">FRESENIOUS FAMILY IN ACTION</div>
        <div class="event-top-details">If It Is To Be, It Is Up To Me<br />Be The Change<br />You Wish To See<br />In The World</div>
        <div class="event-bottom-arrow"><a href="#"><img src="img/arrow-down.png" /></a></div>
    </div>
	<div class="circleBase circle-image">
    	<div class="circle-image-text center-block cwhite">some bottom text here. some bottom text here. some bottom text here. some bottom text here. some bottom text here.</div>
    </div>
</div>
<div class="blue-strip2">
	<div class="container">
    	<div class="row">
        	<div class="col-sm-12">
            	<div class="blue-strip-title floatleft cwhite bs-font">Check Out Our Events</div>
                <a href="#"><div class="lower-blue-strip blue-strip-link floatleft cblue bs-font"><img src="img/arrow-right-yellow.png"/> Add My Country's Event <img src="img/arrow-left-yellow.png"/></div></a>
            </div>            
        </div>
    </div>
</div>
<div class="map-section">
	<div class="container the-width">
    	<div class="row">
        	<div class="col-sm-12">
            	<iframe src="https://mapsengine.google.com/map/embed?mid=zpgmxeNxcXn8.kUtHeAEuxJnI" width="100%" height="800" frameborder="0"></iframe>
            </div>
        </div>
    </div>
</div>
<div class="container the-width"><div class="row"><div class="col-sm-12">
<div class="yellow-strip">

</div>
<div class="grass-bg">
</div>
</div></div></div>


</body>
</html>
